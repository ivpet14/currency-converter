const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const autoprefixer = require('autoprefixer');
const cssMqpacker = require('css-mqpacker');
const cssnano = require('cssnano');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');


module.exports = {
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: "bundle.js",
        publicPath: "/"
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        port: 3000,
        watchContentBase: true,
        progress: true,
        open: false,
        host: '192.168.1.2',
        historyApiFallback: true
    },
    module: {
        rules: [
            {
                test: /\.js$/,  // определяем тип файлов
                exclude: /node_modules/,  // исключаем из обработки папку node_modules
                use: {
                    loader: 'babel-loader',  // определяем загрузчик
                    options: {
                        presets: [["@babel/preset-env",
                            {
                                "targets": {
                                    "node": "10"
                                }
                            }
                        ],[ "@babel/preset-react"]],// используемые плагины
                        plugins: [
                            "@babel/plugin-proposal-class-properties"
                        ]
                    }
                }
            },
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader, "css-loader"
                ]
            },
            {
                test: /\.(sass|scss)$/,
                use:  [
                    MiniCssExtractPlugin.loader,

                    {
                        loader: "css-loader"
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [
                                autoprefixer({
                                    browsers:['ie >= 8', 'last 25 version']
                                }),
                                cssMqpacker,
                                cssnano({
                                    preset: [
                                        'default', {
                                            discardComments: {
                                                removeAll: true,
                                            }
                                        }
                                    ]
                                })
                            ]
                        }
                    },
                    {
                        loader: "sass-loader"

                    }

                ]

            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 1024,
                        name: 'images/[name].[ext]'
                    }
                }]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "style.css",
            chunkFilename: "[name].css"}),
        new HtmlWebpackPlugin({
            template: "./public/index.html"
        })

    ],
    optimization: {
        minimizer: [
            new TerserPlugin({
                test: /\.js(\?.*)?$/i,
            }),
        ],
    }
};