import React from 'react';
import PropTypes from 'prop-types';
import './input.sass';

const Input = ( { name, className, handleChange, ...attrs }) => {
    return (
        <input {...attrs}  name={name} onChange={handleChange}/>
    )
};

Input.propTypes = {
    name: PropTypes.string.isRequired,
    className: PropTypes.string,
    handleChange: PropTypes.func
};

Input.defaultProps = {

};

export default Input;