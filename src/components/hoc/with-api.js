import React from 'react';
import { ApiConsumer } from '../api-context';

const withApi = () => ( Wrapped ) => {
    return ( props ) => (
        <ApiConsumer>
            {
                (api) => <Wrapped {...props} api={api}/>
            }
        </ApiConsumer>
    )
};

export default withApi;