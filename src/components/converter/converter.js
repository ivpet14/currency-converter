import React, { Component } from 'react';
import withApi from '../hoc';
import Select from '../select';
import Input from '../input';
import Button from '../button';
import './converter.sass';


class Converter extends Component {

    state = {
        coins: [],
        loading: true,
        selected1: 'USD',
        selected2: 'RUB',
        input1: 1,
        input2: 'Загрузка ...'
    };

    componentDidMount() {
        this.getCoin();
    }

    getCoin = () => {
        const { api } = this.props;
        api.getCoin()
            .then( coins => {
                this.setState({ coins, loading: false });
                this.calculation();
            })
            .catch( err => console.log(err) );
    };

    handleChange = ( event ) => {
        this.setState({ [event.target.name]: event.target.value });
        this.calculation();

    };

    calculation = () => {
        this.setState((state)=> {
            const {selected1, selected2, input1, coins} = state;
            const coin1 = coins.find(coin => coin.charCode===selected1);
            const coin2 = coins.find(coin => coin.charCode===selected2);
            return {input2: Math.round((coin1.value * input1 / coin1.nominal)/(coin2.value / coin2.nominal) * 10000)/10000}
        });
    };

    swap = () => {
        this.setState({selected1:this.state.selected2,selected2:this.state.selected1});
        this.calculation();
    };

    render() {
        const { coins, selected1, selected2, input1, input2, loading } = this.state;
        const coinsName = coins.map((coin)=> coin.charCode);
        return (
            <div className='calculator'>
                <h2>Конвертер валют</h2>
                {loading ? 'Загрузка ...' : <div className="calculator__text">{`${input1} ${selected1} = ${input2} ${selected2}`}</div>}
                <div className="calculator__box">
                    <div className="calculator__col">
                        <div className="calculator__input-container">
                            <Input name='input1'
                                   handleChange={this.handleChange}
                                   value={input1}
                                   type='number'
                                   step='0.1'
                                   min='0'
                                   max='100000'/>

                            <Select items={coinsName}
                                    name='selected1'
                                    value={selected1}
                                    handleChange={this.handleChange}/>
                        </div>
                        <div className="calculator__input-container">
                            <Input name='input2'
                                   handleChange={this.handleChange}
                                   value={input2}
                                   type='text'
                                   disabled/>

                            <Select items={coinsName}
                                    name='selected2'
                                    value={selected2}
                                    handleChange={this.handleChange}
                            />
                        </div>
                    </div>
                    <Button className='swap'
                            onClick={this.swap} >
                        &nbsp;
                    </Button>
                </div>
            </div>
        )
    }
}



export default withApi()(Converter);