import React, { Component } from 'react';
import ErrorIndicator from '../error-indicator';

export default class ErrorBoundry extends Component {
    state = {
        hassError: false
    };

    componentDidCatch() {
        this.setState({hassError: true});
    }

    render() {
        return (this.state.hassError) ? <ErrorIndicator/> :this.props.children;
    }
}