import React from 'react';
import PropTypes from 'prop-types';
import './button.sass';


const Button = ({children, onClick, className, disabled, active, ...attrs}) => {
    const stylesButton = `btn ${className ? 'btn--'+className: ''} ${active ? active : ''}`;

    return (
        <button
            className={stylesButton}
            disabled={disabled}
            onClick={onClick}
            {...attrs}
        >{children}
        </button>
    )
};

Button.propTypes = {
    children: PropTypes.node,
    onClick: PropTypes.func,
    className: PropTypes.string,
    disabled: PropTypes.bool,
    active: PropTypes.bool
};

Button.defaultProps = {
    children: 'button',
    onClick: () => {},
    className: '',
    disabled: false,
    active: false
};

export default Button;