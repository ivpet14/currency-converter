import React from 'react';
import Converter from '../converter';

const App = () => {

    return (
        <div>
            <Converter/>
        </div>
    )

};

export default App;