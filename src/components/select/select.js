import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import "./select.sass";


const Select = ( { items, handleChange, name, ...attrs } ) => {
    const option = items.map(( item ) => {
        return <option key = { item }
                       value = { item }>
                    {item}
               </option> });

    return (
        <Fragment>
            <select  onChange = { handleChange }
                     name = { name }
                     {...attrs}>
                { option }
            </select>
        </Fragment>
    )
};

Select.propTypes = {
    name: PropTypes.string,
    items: PropTypes.array,
    handleChange: PropTypes.func
};

export default Select