import React from 'react';
import ReactDOM from 'react-dom';
import './index.sass';
import App from './components/app/app';
import ErrorBoundry from './components/error-boundry';
import { ApiProvider } from './components/api-context';
import Api from './services/api';
import ApiTest from './services/api-test';



const test = true;

const api = test ? new ApiTest() : new Api();

ReactDOM.render(
    <ErrorBoundry>
        <ApiProvider value={api}>
            <App />
        </ApiProvider>
    </ErrorBoundry>
    , document.getElementById('root'));