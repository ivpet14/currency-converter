//https://www.cbr-xml-daily.ru/daily_json.js

export default class Api {
    getResource = async () => {
        const res = await fetch('https://www.cbr-xml-daily.ru/daily_json.js');
        if(!res.ok) throw new Error('Не удалось!!!');
        return res.json();
    };

    getCoin = async () => {
        const {Valute} = await this.getResource();
        let arr=[];
        for (let key in Valute) {
            arr = [...arr,this._transform(Valute[key])];
        }
        return [...arr, this._transform({Name:'Рубль',CharCode:'RUB',Value: 1, Previous:1, Nominal: 1})];
    };


    _transform = ( {Name, CharCode, Value, Previous, Nominal} ) => {
        return {
            name: Name,
            charCode: CharCode,
            nominal: Nominal,
            value: Value,
            prevValue: Previous
        }
    }
}