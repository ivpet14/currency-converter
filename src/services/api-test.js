import data from './daily_json.json';

export default class ApiTest {
    getResource = () =>  new Promise((resolve, reject)=>{
        if (data) {
            setTimeout(()=>resolve(data),2000)
        } else {
            const error = new Error('Нет Файла');
            reject(error)
        }
    });


    getCoin = async () => {
        const {Valute} = await this.getResource();
        let arr=[];
        for (let key in Valute) {
            arr = [...arr,this._transform(Valute[key])];
        }
        return [...arr, this._transform({Name:'Рубль',CharCode:'RUB',Value: 1, Previous:1, Nominal: 1})];
    };


    _transform = ( {Name, CharCode, Value, Previous, Nominal} ) => {
        return {
            name: Name,
            charCode: CharCode,
            nominal: Nominal,
            value: Value,
            prevValue: Previous
        }
    }
}